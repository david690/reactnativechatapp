import { lightMode } from "./lightMode";
import { darkMode } from "./darkMode";
import { Appearance } from "react-native";

export const globalThemeName = Appearance.getColorScheme();
export const globalTheme = (globalThemeName == "dark") ? darkMode : lightMode;