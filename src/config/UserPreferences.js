import AsyncStorage from "@react-native-async-storage/async-storage";

export const setPrefference = async (key, value) => {
	try {
		await AsyncStorage.setItem(key, value);
		return true;
	} catch (error) {
		console.log("Error guardando preferencias:", key, error);
		return false;
	}
}

// export const getPrefference = async (key) => {
// 	try {
// 		const value = AsyncStorage.getItem(key);
// 		if(value != null){
// 			console.log("Value:", key, value);
// 			return value;
// 		}
// 	} catch (error) {
// 		console.log("Error obteniendo la preferencia:", key, error);
// 	}
// 	return null;
// }

export const getPrefference = async (key) => {
	return AsyncStorage.getItem(key)
}

export const deletePrefference = async (key) => {
	try {
		await AsyncStorage.removeItem(key);
	} catch (error) {
		console.log("Error al borrar la preferencia:", error);
	}
}
