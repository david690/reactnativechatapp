import React, { useContext, useEffect, useState } from 'react'
import { View, Switch, Text, TouchableOpacity, Alert } from 'react-native';
import { Picker } from "@react-native-picker/picker";
import Icon from 'react-native-vector-icons/Ionicons';
import { auth } from '../config/firebase';
import { signOut } from 'firebase/auth';

import colors from '../styles/colors';
import { getPrefference, setPrefference } from '../config/UserPreferences';
import languages from '../translations/languages';

import { useTranslation } from 'react-i18next';
import i18n from '../translations/i18n';

import themeContext from "../styles/themeContext";
import { EventRegister } from 'react-native-event-listeners';

export const Settings = () => {
	const [switchColorTheme, setswitchColorTheme] = useState(false);
	const [pickerValue, setPickerValue] = useState("es");
	const globalTheme = useContext(themeContext);
	const { t, i18n } = useTranslation();

	useEffect(() => {
		getPrefference("tema").then(tema => {
			if (tema == "light") {
				setswitchColorTheme(false);
			} else {
				setswitchColorTheme(true);
			}
			console.log("Tema actual:", tema);
		});

		getPrefference("idioma").then(idioma => {
			changeLanguage(idioma);
		});
	}, [])


	const onSignOut = () => {
		signOut(auth).catch(error => console.log('Error logging out: ', error));
	};

	const toogleSwitch = () => {
		let value;
		if (switchColorTheme) {
			value = "light";
		} else {
			value = "dark";
		}
		if (setPrefference("tema", value)) {
			console.log("Tema cambiado a:", value);
		}

		// EventRegister.emitEvent("changeTheme", value);

		setswitchColorTheme(!switchColorTheme);
		let mensaje = "";
		if (value == "light") {
			mensaje = t("modoOscuroDesactivado");
		} else {
			mensaje = t("modoOscuroActivado");
		}
		Alert.alert(
			t("aviso"),
			mensaje + t("reinicieAplicacion"),
			[
				{
					text: "Aceptar",
					style: "default"
				}
			]
		);
	}

	const changeLanguage = value => {
		i18n.changeLanguage(value)
			.then(() => setPickerValue(value))
			.catch(error => {
				console.log("Error al actualizar el idioma:", error);
			});
	}

	return (
		<View style={globalTheme.settingsContainer}>
			<Icon name="settings" size={100} color={colors.easternBlue} />

			<View
				style={{ flexDirection: "row", alignItems: "center" }}
			>
				<Text style={globalTheme.textModoOscuro}>{t("modoOscuro")}: </Text>
				<Switch
					trackColor={{ false: "grey", true: "grey" }}
					thumbColor={switchColorTheme ? colors.easternBlue : "grey"}
					onValueChange={toogleSwitch}
					value={switchColorTheme}
				/>
			</View>
			<View style={[globalTheme.pickerContainer, {borderRadius: 4}]}>
				<Picker
				
					selectedValue={pickerValue}
					style={{backgroundColor : (globalTheme.name == "dark") ? colors.bigStone : "white"}}
					onValueChange={(itemValue, itemIndex) => {
						setPickerValue(itemValue);
						if (setPrefference("idioma", itemValue)) {
							console.log("Idioma cambiado a:", itemValue);

							changeLanguage(itemValue);
							// let mensaje = "";

							// if (itemValue == "es") {
							// 	mensaje = "Se ha cambiado el idioma a Español.\n";
							// } else {
							// 	mensaje = "Se ha cambiado el idioma a Inglés.\n";
							// }
							// Alert.alert(
							// 	"Aviso",
							// 	mensaje + "Reinicie la app para ver los cambios.",
							// 	[
							// 		{
							// 			text: "Aceptar",
							// 			style: "default"
							// 		}
							// 	]
							// );
						}
					}}
				>
					{/* <Picker.Item label="Español" value="es" />
					<Picker.Item label="Inglés" value="en" /> */}
					{languages.map((language) => {
						return <Picker.Item label={language.label} value={language.value} key={language.value} style={{color: (globalTheme.name == "dark") ? "white" : "black", backgroundColor: (globalTheme.name == "dark") ? colors.bigStone : "white"}}/>
					})}
				</Picker>
				<TouchableOpacity style={[globalTheme.touchableOpacity, {marginTop: 15}]} onPress={onSignOut}>
					<Text style={globalTheme.touchableOpacityText}> {t("cerrarSesion")}</Text>
				</TouchableOpacity>

			</View>
		</View>
	);
}
