import React, {
	useState,
	useLayoutEffect,
	useCallback,
	useContext,
	useRef,
	useEffect
} from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import {
	collection,
	addDoc,
	orderBy,
	query,
	onSnapshot
} from 'firebase/firestore';
import { auth, database } from '../config/firebase';
import { useNavigation } from '@react-navigation/native';
import colors from '../styles/colors';
import { useTranslation } from 'react-i18next';
import themeContext from "../styles/themeContext";

import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';

import axios from "axios";

Notifications.setNotificationHandler({
	handleNotification: async () => ({
		shouldShowAlert: true,
		shouldPlaySound: true,
		shouldSetBadge: true,
	}),
});

export const Chat = () => {
	const [expoPushToken, setExpoPushToken] = useState('');
	const [notification, setNotification] = useState(false);
	const notificationListener = useRef();
	const responseListener = useRef();

	useEffect(() => {

		registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

		// This listener is fired whenever a notification is received while the app is foregrounded
		notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
			setNotification(notification);
		});

		// This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
		responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
			console.log(response);
		});

		return () => {
			Notifications.removeNotificationSubscription(notificationListener.current);
			Notifications.removeNotificationSubscription(responseListener.current);
		};
	}, []);

	async function sendPushNotification(expoPushToken, mensaje) {
		const message = {
			to: expoPushToken,
			sound: Platform.OS !== "android" ? "default" : "default",
			title: 'Chat Local\'s Atlas',
			body: mensaje,
			data: { someData: 'goes here' },

		};
		await fetch('https://exp.host/--/api/v2/push/send', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Accept-encoding': 'gzip, deflate',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(message),
		});
	}

	async function registerForPushNotificationsAsync() {
		let token;
		if (Device.isDevice) {
			const { status: existingStatus } = await Notifications.getPermissionsAsync();
			let finalStatus = existingStatus;
			console.log(finalStatus)
			if (existingStatus !== 'granted') {
				const { status } = await Notifications.requestPermissionsAsync();
				finalStatus = status;
			}
			if (finalStatus !== 'granted') {
				alert('Failed to get push token for push notification!');
				return;
			}
			token = (await Notifications.getExpoPushTokenAsync()).data;
		} else {
			alert('Must use physical device for Push Notifications');
		}

		if (Platform.OS === 'android') {
			Notifications.setNotificationChannelAsync('default', {
				name: 'default',
				importance: Notifications.AndroidImportance.MAX,
				vibrationPattern: [0, 250, 250, 250],
				lightColor: '#FF231F7C',
			});
		}
		return token;
	}

	const globalTheme = useContext(themeContext);
	const [messages, setMessages] = useState([]);
	const [myMessage, setMyMessage] = useState(false);
	const navigation = useNavigation();
	const { t, i18n } = useTranslation();

	useLayoutEffect(() => {

		const collectionRef = collection(database, 'chats');
		const q = query(collectionRef, orderBy('createdAt', 'desc'));
		const unsubscribe = onSnapshot(q, querySnapshot => {
			console.log('querySnapshot unsusbscribe');

			sendPushNotification(expoPushToken, "Nuevo mensaje.");

			setMessages(
				querySnapshot.docs.map(doc => ({
					_id: doc.data()._id,
					createdAt: doc.data().createdAt.toDate(),
					text: doc.data().text,
					user: doc.data().user,
					image: doc.data().image,
					msg: doc.data().text
				}))
			);


		});
		return unsubscribe; 
	}, []);

	const onSend = useCallback((messages = []) => {
		setMessages(previousMessages =>
			GiftedChat.append(previousMessages, messages)
		);
		const { _id, createdAt, text, user } = messages[0];
		addDoc(collection(database, 'chats'), {
			_id,
			createdAt,
			text,
			user
		});
	}, []);

	return (
		<GiftedChat
			messages={messages}
			showAvatarForEveryMessage={false}
			showUserAvatar={false}
			onSend={messages => onSend(messages)}
			messagesContainerStyle={{
				backgroundColor: (globalTheme.name == "dark") ? colors.bigStone : "white"
			}}
			textInputStyle={{
				backgroundColor: "white",
				borderRadius: 4,
			}}
			user={{
				_id: auth?.currentUser?.email,
				avatar: 'https://i.pravatar.cc/300',
				name: auth?.currentUser?.email.split("@")[0]
			}}
			renderUsernameOnMessage={true}
			placeholder={t("escribeMensaje")}
			isTyping={true}
			alwaysShowSend={true}
			renderAvatar={null}
			dateFormat="LLL"
		/>
	);
}

