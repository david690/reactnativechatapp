import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, TextInput, Image, SafeAreaView, TouchableOpacity, Alert } from "react-native";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../config/firebase";
import { lightMode } from "../styles/lightMode";
import { useTranslation } from 'react-i18next';
import { getPrefference } from "../config/UserPreferences";

export default function Signup({ navigation }) {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const globalTheme = lightMode;

	const { t, i18n } = useTranslation();
	useEffect(() => {
		getPrefference("idioma").then(idioma => {
			i18n.changeLanguage(idioma)
				.then(() => console.log("val", idioma))
				.catch(error => {
					console.log("Error al actualizar el idioma:", error);
				});
		});
	}, []);

	const onHandleSignup = () => {
		if (email !== "" && password !== "") {
			createUserWithEmailAndPassword(auth, email, password)
				.then(() => console.log("Cuenta creada del usuario:", email))
				.catch((err) => Alert.alert("Login error", err.message));
		}
	};
	return (
		<View style={globalTheme.container}>
			<SafeAreaView style={globalTheme.form}>
				<Text style={globalTheme.titulo}>{t("crearCuenta")}</Text>
				<Image source={require('../../assets/logo.png')} style={{ alignSelf: "center", marginVertical: 20 }} />

				<Text style={[globalTheme.titulo, { color: "black", fontSize: 24 }]}>Local's Atlas</Text>

				<TextInput
					style={globalTheme.input}
					placeholder={t("correoElectronico")}
					autoCapitalize="none"
					textContentType="emailAddress"
					autoFocus={true}
					value={email}
					onChangeText={(text) => setEmail(text)}
				/>
				<TextInput
					style={globalTheme.input}
					placeholder={t("contraseña")}
					autoCapitalize="none"
					autoCorrect={false}
					secureTextEntry={true}
					textContentType="password"
					value={password}
					onChangeText={(text) => setPassword(text)}
				/>

				<TouchableOpacity style={globalTheme.touchableOpacity} onPress={onHandleSignup}>
					<Text style={{ fontWeight: 'bold', color: '#fff', fontSize: 18 }}>{t("crearCuenta")}</Text>
				</TouchableOpacity>

				<View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', alignSelf: 'center' }}>
					<TouchableOpacity onPress={() => navigation.navigate("Login")}>
						<Text style={{ color: '#f57c00', fontWeight: '600', fontSize: 14 }}>{t("iniciarSesion")}</Text>
					</TouchableOpacity>
				</View>
			</SafeAreaView>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
	},
	title: {
		fontSize: 36,
		fontWeight: 'bold',
		color: "orange",
		alignSelf: "center",
		paddingBottom: 24,
	},
	input: {
		backgroundColor: "#F6F7FB",
		height: 58,
		marginBottom: 20,
		fontSize: 16,
		borderRadius: 10,
		padding: 12,
	},
	backImage: {
		width: "100%",
		height: 340,
		position: "absolute",
		top: 0,
		resizeMode: 'cover',
	},
	whiteSheet: {
		width: '100%',
		height: '75%',
		position: "absolute",
		bottom: 0,
		backgroundColor: '#fff',
		borderTopLeftRadius: 60,
	},
	form: {
		flex: 1,
		justifyContent: 'center',
		marginHorizontal: 30,
	},
	button: {
		backgroundColor: '#f57c00',
		height: 58,
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 40,
	},
});